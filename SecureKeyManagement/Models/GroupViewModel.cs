﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SecureKeyManagement.Models
{
    public class GroupViewModel
    {
        public Group Group { get; set; }
        public IEnumerable<SelectListItem> AllDevices { get; set; }

        private int[] _selectedDevices;
        public int[] SelectedDevices
        {
            get
            {
                if (_selectedDevices == null)
                {
                    if (Group == null)
                        return null;
                    _selectedDevices = Group.Devices.Select(m => m.ID).ToArray();
                }
                return _selectedDevices;
            }
            set { _selectedDevices = value; }
        }
    }
}