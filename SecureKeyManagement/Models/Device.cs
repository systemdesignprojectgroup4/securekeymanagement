﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureKeyManagement.Models
{
    public class Device
    {
        public Device()
        {
            this.Groups = new HashSet<Group>();
        }

        public int ID { get; set; }
        public string Key { get; set; }

        public virtual ICollection<Group> Groups { get; set; }
    }
}