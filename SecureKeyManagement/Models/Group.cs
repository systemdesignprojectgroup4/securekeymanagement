﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecureKeyManagement.Models
{
    public class Group
    {
        public Group()
        {
            this.Devices = new HashSet<Device>();
        }

        public int ID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Device> Devices { get; set; }
    }
}