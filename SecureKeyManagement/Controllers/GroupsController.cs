﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SecureKeyManagement.DAL;
using SecureKeyManagement.Models;

namespace SecureKeyManagement.Controllers
{
    public class GroupsController : Controller
    {
        private DataContext db = new DataContext();

        // GET: Groups
        public ActionResult Index()
        {
            return View(db.Groups.ToList());
        }

        // GET: Groups/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = db.Groups.Find(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        // GET: Groups/Create
        public ActionResult Create()
        {
            var viewModel = new GroupViewModel();

            var allDevicesList = db.Devices.ToList();
            viewModel.AllDevices = allDevicesList.Select(o => new SelectListItem
            {
                Text = o.Key,
                Value = o.ID.ToString()
            });

            return View(viewModel);
        }

        // POST: Groups/Create
        // Per proteggere da attacchi di overposting, abilitare le proprietà a cui eseguire il binding. 
        // Per ulteriori dettagli, vedere https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GroupViewModel viewModel)
        {
            var group = viewModel.Group;
            if (ModelState.IsValid)
            {
                foreach (var idDevice in viewModel.SelectedDevices)
                {
                    var device = db.Devices.FirstOrDefault(f => f.ID == idDevice);
                    if (device != null)
                    {
                        group.Devices.Add(device);
                        device.Groups.Add(group);
                    }
                }
                db.Groups.Add(group);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(group);
        }

        // GET: Groups/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var viewModel = new GroupViewModel
            {
                Group = db.Groups.Include(i => i.Devices).First(i => i.ID == id),
            };

            if (viewModel.Group == null)
                return HttpNotFound();

            var allDevicesList = db.Devices.ToList();
            viewModel.AllDevices = allDevicesList.Select(o => new SelectListItem
            {
                Text = o.Key,
                Value = o.ID.ToString()
            });

            return View(viewModel);
        }

        // POST: Groups/Edit/5
        // Per proteggere da attacchi di overposting, abilitare le proprietà a cui eseguire il binding. 
        // Per ulteriori dettagli, vedere https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GroupViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(viewModel.Group).State = EntityState.Modified;
                var group = viewModel.Group;
                var idDevices = viewModel.SelectedDevices;
                var allDevices = db.Devices.ToList();
                foreach (var device in allDevices)
                {
                    group.Devices.Remove(device);
                    device.Groups.Remove(group);
                }

                foreach (var idDevice in idDevices)
                {
                    var device = db.Devices.FirstOrDefault(f => f.ID == idDevice);
                    if (device != null)
                    {
                        group.Devices.Add(device);
                        device.Groups.Add(group);
                    }
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(viewModel.Group);
        }

        private void UpdateDevicesOfGroup(Group group, int[] idDevices)
        {
            var allDevices = db.Devices.ToList();
            foreach (var device in allDevices)
            {
                group.Devices.Remove(device);
            }

            foreach (var idDevice in idDevices)
            {
                var device = db.Devices.FirstOrDefault(f => f.ID == idDevice);
                if (device != null)
                    group.Devices.Add(device);
            }
        }

        // GET: Groups/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = db.Groups.Find(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        // POST: Groups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Group group = db.Groups.Find(id);
            db.Groups.Remove(group);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
