namespace SecureKeyManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Device",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Key = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Group",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.GroupDevice",
                c => new
                    {
                        Group_ID = c.Int(nullable: false),
                        Device_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Group_ID, t.Device_ID })
                .ForeignKey("dbo.Group", t => t.Group_ID, cascadeDelete: true)
                .ForeignKey("dbo.Device", t => t.Device_ID, cascadeDelete: true)
                .Index(t => t.Group_ID)
                .Index(t => t.Device_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GroupDevice", "Device_ID", "dbo.Device");
            DropForeignKey("dbo.GroupDevice", "Group_ID", "dbo.Group");
            DropIndex("dbo.GroupDevice", new[] { "Device_ID" });
            DropIndex("dbo.GroupDevice", new[] { "Group_ID" });
            DropTable("dbo.GroupDevice");
            DropTable("dbo.Group");
            DropTable("dbo.Device");
        }
    }
}
