﻿using SecureKeyManagement.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace SecureKeyManagement.DAL
{
    public class DataContext : DbContext
    {
        public DataContext() : base("DataContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public System.Data.Entity.DbSet<SecureKeyManagement.Models.Device> Devices { get; set; }

        public System.Data.Entity.DbSet<SecureKeyManagement.Models.Group> Groups { get; set; }
    }
}